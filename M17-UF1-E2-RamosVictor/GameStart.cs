﻿using System;
using System.Collections.Generic;
using System.Text;
using MyFirstProgram;
using GameTools;

namespace M17_UF1_E2_RamosVictor
{
    internal class GameStart : GameEngine
    {
        MatrixRepresentation matriz = new MatrixRepresentation();
        Random rnd = new Random();
        int tmp = 0;

        protected override void Start()
        {
            //var matriu = new MatrixRepresentation(20,20);

            //matriu.CleanTheMatrix();
            // matriz.clippingMatrix(matriu.blankMatrix());
            matriz.initMatrix(10, 10);
            matriz.CleanMatrix();
            matriz.CleanTheMatrix();
            matriz.blankMatrix();

         
            

        }

        protected override void Update()
        {
            
            matriz.printMatrix();

            for (int i = matriz.TheMatrix.GetLength(0) - 1; i > 0; i--)
            {
                for (int j = matriz.TheMatrix.GetLength(1) - 1; j >= 0; j--)
                {
                    char temp = matriz.TheMatrix[i - 1, j];
                    matriz.TheMatrix[i - 1, j] = matriz.TheMatrix[i, j];
                    matriz.TheMatrix[i, j] = temp;
                }
            }


            for (int i = 0; i < matriz.TheMatrix.GetLength(0); i++)
            {
                matriz.TheMatrix[i, tmp] = (char)('a' + rnd.Next(0, 26));
            }

            if (tmp < matriz.TheMatrix.GetLength(0) - 1) tmp++;
            else tmp = 0;


        }

        protected override void Exit()
        {
            base.Exit();


        }

    }
}
